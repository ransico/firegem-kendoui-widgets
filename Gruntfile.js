// Generated on 2015-02-16 using generator-angular 0.10.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'app',
    dist: 'dist'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    yeoman: appConfig,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      js: {
        files: ['<%= yeoman.app %>/scripts/*.js'],
        //tasks: ['newer:jshint:beforeconcat']
        tasks: ['build']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      styles: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.css', '<%= yeoman.app %>/styles/{,*/}*.less'],
        tasks: ['less']
      },
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      beforeconcat: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/*.js'
        ],
      afterconcat: ['<%= yeoman.dist %>/firegem.widgets.js']
    },

    less: {
      dev: {
        files: [
          {
            expand: true,
            cwd: '<%= yeoman.app %>/styles',
            src: ['firegem.widgets.less'],
            dest: '<%= yeoman.dist %>',
            ext: '.widgets.css'
          }
        ]
      }
    },

    concat: {
      dist: {
        src: ['<%= yeoman.app %>/scripts/*.js'],
        dest: '<%= yeoman.dist %>/firegem.widgets.js'
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= yeoman.dist %>/{,*/}*',
            '!<%= yeoman.dist %>/.git{,*/}*'
          ]
        }]
      }
    },

    uglify: {
      dist: {
        files: {
          '<%= yeoman.dist %>/firegem.widgets.min.js': [
            '<%= yeoman.dist %>/firegem.widgets.js'
          ]
        }
      }
    },
  });

  grunt.registerTask('build', [
    'jshint:beforeconcat',
    'clean:dist',
    'concat',
    'less',
    'jshint:afterconcat',
    'uglify'
  ]);

  grunt.registerTask('default', [
    'build'
  ]);
};

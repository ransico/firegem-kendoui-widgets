# listOfValues

## Overview
This control is based on the Java EE List of Values. It is an input box with a magnifying glass next to it, which when pressed opens a 'Search and Select' dialog box.

The search dialog offers configurable fields that can be used to filter a kendo datasource. The results of the filtered search are shown in a grid in the dialog.

The user can select an item in this grid (using a select button, or double clicking the row) which will close the window and populate the input box with the configurable 'primary' attribute of the selected row.

## Methods

### selected()

Returns the row object of the current selection in the results grid.

### value()

Returns the current value of the input box.

### choose()

If there are items in the results grid, this function will 'select' the currently highlighted row. This populates the _value_ of the widget with the row value as bound in the _valueField_ option.

### reset()

Reset the values of the filter options to the default values provided in the widget options, and clears any validation messages.

This does not clear the results grid.

### filterFromInput(callback)

Attempt to filter the grid (even if the window is hidden), based on the current value of the input box. The input box value is bound to the filter through the use of the _valueField_ configuration option.

Once the grid is populated with the results of the service call, _callback_ is invoked with:

    callback(null, numberOfResults);

If an error occurs, such as the service timing out, _callback_ is invoked with:

    callback("errorMessage", 0);

### filter(callback)

Attempt to filter the grid (even if the window is hidden), based on the current values of the filter inputs.

Once the grid is populated with the results of the service call, _callback_ is invoked with:

    callback(null, numberOfResults);

If an error occurs, such as the service timing out, _callback_ is invoked with:

    callback("errorMessage", 0);

### close()

Closes the search and select window, if it is open.

### open()

First, calls reset() to clear the search filters.

Second, Opens and centers the search and select window, if it is closed.

### setDataSource(dataSource)

Changes the data source that is used by the widget for retrieving search results.

## Events

The widget fires some standard kendoUI events. They can be subscribed to either via configuration (in the Options object) or after the widget has been created, using the _bind_ function.

### change

Fired when the user selects a value from the table, or when the value of the input field changes (on blur).

Single parameter: the widget instance.

### databinding

Fired when the widget starts refreshing its DOM

### databound

Fired when the widget completes refreshing its DOM

## Configuration

### autoBind
_Boolean_

If set to false the widget will not bind to the data source during initialization. In this case data binding will occur when the change event of the data source is fired. By default the widget will bind to the data source specified in the configuration.

### autoFocusTimeout
_Integer_

In milliseconds

### enableFocus
_Boolean_

Enable focusing input box and table after results are found. Causes problems when the page height exceeds browser clientHeight, so should be left at the default: false.

### fieldColumns
_Object_

Which fields are used for filtering the datasource. A form will be rendered above the grid, based on the configuration of these fields.

    {
        'columnName': {
            name: string     (field)
            label: string    (human readable display name)
            tooltip: string  (help text)
            required: string ('SelectivelyRequired', 'Required', 'Optional')
            defaultOperator: string  (any kendoui datasource operator)
            allowedOperators: Array of string
            format: string   (kendoui validation format)
            editor: object   (super advanced: kendoui editor type - a function that creates an editor element.)
        }
    }

Where the valid values for _required_ are:

| Required            | Search form cannot be submitted unless this field is provided  |
| Optional            | Is not required for a search to take place                     |
| SelectivelyRequired | At least one field with 'selectivelyRequired' must be provided |

Where the valid values for _defaultOperator_ are:

| Operator       | Description              | Valid for data types |
|----------------|--------------------------|----------------------|
| eq             | equals                   | all                  |
| neq            | not equals               | all                  |
| gt             | greater than             | numbers, dates       |
| gte            | greater than or equal to | numbers, dates       |
| lt             | less than                | numbers, dates       |
| lte            | less than or equal to    | numbers, dates       |
| startswith     | starts with              | strings              |
| endswith       | ends with                | strings              |
| contains       | contains                 | strings              |
| doesnotcontain | does not contain         | strings              |
| isnull         | is null                  | all                  |
| isnotnull      | is not null              | all                  |


### filterValues
_Object_

Initial values of the search filters, used for filtering the grid dataSource. Once the control is initialized, these values are ignored and not kept up to date (unless it is passed as an observable object):

    <input id="vmLookup" type="text" />

    var element = $('#vmLookup');
    element.kendoLookup({
        ...
        filterValues: kendo.observable({
            primaryKey: '1144'
        });
    });

### gridColumns
_Array_

The configuration of the grid columns. An array of JavaScript objects or strings. A JavaScript objects are interpreted as column configurations. Strings are interpreted as the field to which the column is bound. The grid will create a column for every item of the array.

See KendoUI Grid API documentation.

### innerWrapClass
_String_

### meta.attrNames
Not currently used.

### meta.autoExecute

### meta.derivedAttrNames
Not currently used.

### meta.displayCriteria

### meta.listDisplayAttrNames
Not currently used.

### meta.multiOperators
Not currently used.

### meta.name
Not currently used.

### openOnInvalid
_String_

If after a search is performed there is not exactly 1 valid result, and the window is not open, then the window will be opened and search fields pre-populated with the query.

This can occur if a manual call to the filter function results in a number of results other than 1:

    <input id="vmLookup" type="text" />

    var element = $('#vmLookup');
    element.kendoLookup({
        ...
        openOnInvalid: true
    });

    element.data('kendoLookup').filterFromInput(function(error, numResults) {
        // if numResults !== 1, then the window will be opened prior to this callback being fulfilled.
    });

### notificationOnInvalid
_String_

If after a search is performed there is not exactly 1 valid result, and the window is not open, then a notification will be displayed next to the input field describing this.

This can occur if a manual call to the filter function results in a number of results other than 1:

    <input id="vmLookup" type="text" />

    var element = $('#vmLookup');
    element.kendoLookup({
        ...
        notificationOnInvalid: true
    });

    element.data('kendoLookup').filterFromInput(function(error, numResults) {
        // if numResults !== 1, then notification displayed prior to this callback being fulfilled.
    });

### outerWrapClass
_String_

CSS Class that will be added to the outer wrapper of the control. Defaults to:

    k-widget vm-lookup

### placeholder
_String_

Placeholder text that is displayed in the input textbox, when there is no entered value.

### showInput
_Boolean_

Toggles the visibility of the text input portion of the control. This setting has no bearing on the visibility of the magnifier button.

### value
_String_

Initial value of the control.

### valuePrimitive
_Boolean_

Defaults to: false

Specifies the value binding behavior for the widget. If set to true, the View-Model field will be updated with the selected item value field, based on the _valueField_ option. If set to false, the View-Model field will be updated with the selected item row object.

### valueField
_String_

When selecting a row from the results grid, the field with key equal to _valueField_ will be used for the value of the input textbox. In other words, _valueField_ is the name of the key field of the row.

### windowHeight
_Integer_

The height of the popup window, in pixels. Defaults to: 450

### windowTitle
_String_

The title of the popup window. Defaults to: 'Search and Select'

### windowWidth
_Integer_

The width of the popup window, in pixels. Defaults to: 600
(function() {

    'use strict';
    /* global define */

(function(f, define){
    define([], f);
})(function(){

    (function($) {
        var kendo = window.kendo = window.kendo || { cultures: {} };

        /** KENDO EXTENSION - Expander Box **/

        var DOUBLECLICK = 'dblclick',
            CLICK = 'click',
            EXPANDERROLE_QRY = '[data-role="expander"]',
            EXPANDERHEADERTEXT_ATTR = 'data-header-text',
            EXPANDERHEADERROLE_ATTR = 'expander-header',
            EXPANDERHEADERROLE_QRY = '[data-role="expander-header"]',
            EXPANDERHEADERICONROLE_ATTR = 'expander-icon',
            EXPANDERHEADERICONROLE_QRY = '[data-role=expander-icon]',
            EXPANDERHEADERACTIONSROLE = '[data-role="expander-header-actions"]',
            EXPANDERCONTENTROLE = '[data-role="expander-content"]',
            EXPANDERCLASS = 'fg-expander',
            EXPANDERHEADERCLASS = 'fg-expander-header',
            EXPANDERHEADERACTIONSCLASS = 'fg-expander-actions',
            EXPANDERCONTENTCLASS = 'fg-expander-content',
            EXPANDERVISIBLECLASS = 'fg-expander-open',
            EXPANDERHIDDENCLASS = 'fg-expander-closed',
            EXPANDERHEADERICON_CLASS = 'k-sprite fg-expander-icon',
            ns = '.expander';

        var Expander = kendo.ui.Widget.extend({
            init: function(element, options) {
                var self = this,
                    $elem,
                    expandedVal,
                    $header,
                    $headerH2,
                    $headerActions,
                    headerText;

                // Base initialization
                kendo.ui.Widget.fn.init.call(this, element, options);

                /*
                Input HTML
                <div data-role="expander"
                     data-expanded="false"
                     data-header-text="some arbitrary title (can be html)">

                    <!-- Optional: provide some actions -->
                    <div data-role="expander-header-actions">
                        <a data-role="button" data-bind="click: onAddLineClick" data-icon="fff-add">Add Line</a>
                    </div>

                    <!-- This div contains the content that will have its visibility toggled.
                    <div data-role="expander-content">
                        <!-- Arbitrary content here -->
                    </div>
                </div>


                Output HTML

                 */

                $elem = $(element);

                $header = $elem.find(EXPANDERHEADERROLE_QRY);
                if ($header.size() === 0) {
                    $header = $('<div />')
                        .attr('data-role', EXPANDERHEADERROLE_ATTR);
                    $elem.prepend($header);
                }

                headerText = $.trim($elem.attr(EXPANDERHEADERTEXT_ATTR));
                if (headerText.length > 0) {
                    $headerH2 = $header.find('h2');
                    if ($headerH2.size() === 0) {
                        $headerH2 = $('<h2 />');
                        $header.prepend($headerH2);
                    }
                    $headerH2.html(headerText);
                }

                // Expander icon
                $header.prepend(
                    $('<span />')
                        .attr('data-role', EXPANDERHEADERICONROLE_ATTR)
                        .addClass(EXPANDERHEADERICON_CLASS)
                );

                $headerActions = $elem.find(EXPANDERHEADERACTIONSROLE);
                if ($headerActions.size()) {
                    // Move the header actions to the Header parent element.
                    $header.prepend($headerActions);
                }

                // Apply our styling
                $elem.addClass(EXPANDERCLASS);
                $header.addClass(EXPANDERHEADERCLASS);
                $headerActions.addClass(EXPANDERHEADERACTIONSCLASS);
                $elem.find(EXPANDERCONTENTROLE).addClass(EXPANDERCONTENTCLASS);

                // Initially hidden?
                expandedVal = $elem.data('expanded') === true;
                if (!expandedVal) {
                    $elem.addClass(EXPANDERHIDDENCLASS)
                        .find(EXPANDERCONTENTROLE)
                        .hide();
                }

                self.wrapper = $elem;

                self._initEvents();

                self.refresh();
            },

            _initEvents: function() {

                var $elem = this.wrapper,
                    $target,
                    animTime = this.options.animTime,
                    handleExpand;

                handleExpand = function(evt) {
                    $elem = $(this);

                    $elem = $elem.is(EXPANDERROLE_QRY) ? $elem : $elem.parents(EXPANDERROLE_QRY).first();
                    $target = $elem.find(EXPANDERCONTENTROLE);
                    if ($target.is(':visible')) {
                        $elem
                            .removeClass(EXPANDERVISIBLECLASS)
                            .addClass(EXPANDERHIDDENCLASS);
                        $target.slideUp(animTime);
                    }
                    else {
                        $elem.removeClass(EXPANDERHIDDENCLASS)
                            .addClass(EXPANDERVISIBLECLASS);
                        $target.slideDown(animTime);
                    }

                    if (evt.preventDefault) {
                        evt.preventDefault();
                    }
                };

                // Attach events
                $elem.find(EXPANDERHEADERROLE_QRY).on(DOUBLECLICK + ns, handleExpand);
                $elem.find(EXPANDERHEADERICONROLE_QRY).on(CLICK + ns, handleExpand);
            },

            destroy: function() {

                this.wrapper.off(ns);

                kendo.ui.Widget.fn.destroy.call(this);
            },

            refresh: function() {

            },

            options: {
                name: 'Expander',
                outerWrapClass: 'k-widget vm-lookup',
                innerWrapClass: 'k-picker-wrap k-state-default',
                animTime: 200
            }
        });
        kendo.ui.plugin(Expander);

    })(window.kendo.jQuery);

    return window.kendo;

}, typeof define === 'function' && define.amd ? define : function(_, f){ f(); });

})();
(function() {

    'use strict';
    /* global define */

(function(f, define){
    define([], f);
})(function(){

    (function($) {
        var kendo = window.kendo = window.kendo || { cultures: {} };
        /** KENDO EXTENSION - Magic Lookup **/

        var DATABINDING = 'dataBinding',
            DATABOUND = 'dataBound',
            CHANGE = 'change',
            SELECT_INPUT = '[data-ml="input"]',
            SELECT_BUTTON = '[data-ml="button"]',
            SELECT_BUTTON_OPEN = '[data-action="open"]',
            SELECT_BUTTON_CLOSE = '[data-action="close"]',
            SELECT_BUTTON_FILTER = '[data-action="filter"]',
            SELECT_BUTTON_RESET = '[data-action="reset"]',
            SELECT_BUTTON_OK = '[data-action="choose"]',
            SELECT_FIELDS = '[data-ml="fields"]',
            INVALID_DS_MSG = 'Invalid data source provided. Must contain a valid Schema.',
            TOO_MANY_RESULTS_MSG = 'More than one matching record found.',
            NO_RESULTS_MSG = 'No matching records found.',
            FOCUSED = 'k-state-focused',
            DISABLED = 'k-state-disabled',
            HOVER = 'k-state-hover',
            CLS_DETAIL_FIELDS = 'vm-lu-detail-fields',
            CLS_DETAIL_FIELD = 'vm-lu-detail-field',
            CLS_DETAIL_FIELD_VALUE = 'vm-lu-detail-field-value',
            CLS_DETAIL_FIELD_REQUIRED = 'vm-lu-detail-field-required',
            CLS_DETAIL_FIELD_SELECTIVEREQUIRED = 'vm-lu-detail-field-selectiverequired',
            CLS_FILTER_PANEL = 'vm-lu-filter-panel',
            CLS_FILTER_GRID = 'vm-lu-filter-grid',
            CLS_COMMANDS = 'vm-lu-commands',
            ns = '.lookup',
            allOperators = [
                { code: 'eq', displayName: 'Equals' },
                { code: 'neq', displayName: 'Not Equals' },
                { code: 'gt', displayName: 'Greater than' },
                { code: 'gte', displayName: 'Greater than or equals' },
                { code: 'lt', displayName: 'Less than' },
                { code: 'lte', displayName: 'Less than or equals' },
                { code: 'startswith', displayName: 'Starts With' },
                { code: 'endswith', displayName: 'Ends With' },
                { code: 'contains', displayName: 'Contains' },
                { code: 'doesnotcontain', displayName: 'Does not Contain' },
                { code: 'isnull', displayName: 'Is Null' },
                { code: 'isnotnull', displayName: 'Is not Null' }
                ];

        var Lookup = kendo.ui.Widget.extend({
            init: function(element, options) {
                var self = this,
                    pickerWrap;

                // Base initialization
                kendo.ui.Widget.fn.init.call(this, element, options);

                element = self.element;
                options = self.options;

                if (!options.valueField) { throw 'valueField option must not be blank'; }

                options.placeholder = options.placeholder || element.attr('placeholder');
                if (kendo.support.placeholder) {
                    element.attr('placeholder', options.placeholder);
                }

                pickerWrap = element.wrap('<div data-ml="wrapper" class="' + options.innerWrapClass + '" />').parent();
                self.wrapper = pickerWrap.wrap('<div class="' + options.outerWrapClass + '" />');

                element.addClass('k-input');
                element.attr('data-ml', 'input');

                if (options.valuePrimitive) {
                    self._oldVal = options.value;
                }
                else if (options.value) {
                    self._oldVal = options.value[options.valueField] || '';
                }
                else {
                    self._oldVal = '';
                }

                element.val(self._oldVal);

                this._selected = null;

                self.button = $('<span />')
                    .attr('class', 'k-select')
                    .attr('data-ml', 'button')
                    .attr('data-action', 'open')
                    .html('<span class="k-icon k-i-search"></span>');
                self.wrapper.append(self.button);

                self._initDataSource();
                self._initEvents();

                self.refresh();
            },

            _initWindow: function() {
                var self = this,
                    wdw,
                    wrapper,
                    filterPanel,
                    filterButtons,
                    resizeProxy = $.proxy(self._resizeGrid, self);

                wdw = $('<div />')
                    .kendoWindow({
                         actions: ['Close'],
                         title: self.options.windowTitle,
                         modal:true,
                         width: self.options.windowWidth,
                         height: self.options.windowHeight,
                         resize: resizeProxy,
                         activate: function() {
                            if (self.dataSource.total() === 0) {
                                // If no results from the search, focus the first field.
                                var input = $(this.element).find(SELECT_FIELDS).find('input:eq(0)');
                                if (self.options.enableFocus) {
                                    input.focus();
                                }
                            } else {
                                // If there were results, focus the table.
                                self._grid.select(null);
                                if (self.options.enableFocus) {
                                    self._grid.table.focus();
                                }
                            }
                             self._resizeGrid();
                         },
                         deactivate: function() {
                            if (self.options.enableFocus) {
                                setTimeout(function() { self.wrapper.find(SELECT_BUTTON_OPEN).focus(); }, 0);
                            }
                         }
                    });
                self.window = wdw.data('kendoWindow');

                filterButtons = $('<div class="'+CLS_DETAIL_FIELDS+'" />')
                    .append($('<div class="'+CLS_DETAIL_FIELD+'">')
                        .append('<label />')
                        .append($('<div class="'+CLS_DETAIL_FIELD_VALUE+'">')
                            .append('<button class="k-button" data-ml="button" data-action="filter">Filter</button> ')
                            .append('<button class="k-button" data-ml="button" data-action="reset">Reset</button>')
                        )
                    );

                filterPanel = $('<div class="'+CLS_FILTER_PANEL+'" />')
                    .append($('<div data-ml="fields" class="'+CLS_DETAIL_FIELDS+'">' + INVALID_DS_MSG + '<div>')
                        .kendoValidator({
                            rules: {
                                requiredFields: $.proxy(self._validateFields, self)
                            },
                            messages: {
                                requiredFields: $.proxy(self._validateFieldsMessage, self)
                            },
                            validateOnBlur: false
                        })
                    )
                    .append(filterButtons);

                wrapper = $('<div style="height: 100%; overflow: hidden;"/>')
                    .append(filterPanel)
                    .appendTo(wdw);

                self._grid = $('<div class="'+CLS_FILTER_GRID+'" />').appendTo(wrapper)
                    .kendoGrid({
                        autoBind: false,
                        columns: self.options.gridColumns,
                        filterable: false,
                        sortable: true,
                        selectable: 'row',
                        navigatable: true,
                        dataSource: self.dataSource,
                        scrollable: { virtual: true },
                        change: $.proxy(self._gridChange, self)
                    })
                    .data('kendoGrid');

                $('<div class="'+CLS_COMMANDS+'" />')
                    .append('<button class="k-button" data-ml="button" data-action="choose">Use Selection</button>')
                    .append(' <button class="k-button" data-ml="button" data-action="close">Cancel</button>')
                    .appendTo(wrapper);
            },

            _initEvents: function() {

                var self = this,
                    $elem = $(self._getInput());

                if ($elem.length !== 1) {
                    return;
                }

                self.wrapper
                    .on('focus' + ns, SELECT_INPUT, function () {
                        $elem.addClass(FOCUSED);
                    })
                    .on('blur' + ns, SELECT_INPUT, function () {
                        self._inputChange();
                        $elem.removeClass(FOCUSED);
                    })
                    .on('mouseover' + ns, function() {
                        $(this).addClass(HOVER);
                    })
                    .on('mouseout' + ns, function() {
                        $(this).removeClass(HOVER);
                    })
                    .on('click' + ns, SELECT_BUTTON, function(evt) {
                        self.open();
                        evt.preventDefault();
                    });
            },

            _initWindowEvents: function() {
                var self = this;

                self.window.element
                    .on('click' + ns, SELECT_BUTTON, function(evt) {
                        var btn = $(this);

                        if (btn.is('.' + DISABLED)) {
                            evt.preventDefault();
                            return;
                        }

                        if (btn.is(SELECT_BUTTON_FILTER)) {
                            self.filter();
                        }
                        else if (btn.is(SELECT_BUTTON_RESET)) {
                            self.reset();
                        }
                        else if (btn.is(SELECT_BUTTON_OK)) {
                            self.choose();
                        }
                        else if (btn.is(SELECT_BUTTON_CLOSE)) {
                            self.close();
                        }
                        else {
                            return;
                        }

                        evt.preventDefault();
                    })
                    .on('keydown' + ns, '.k-grid', function(evt) {
                        if (evt.keyCode === kendo.keys.ENTER) {
                            var grid = self._grid,
                                td = grid.element.find('td.k-state-focused'),
                                isSelected = td.parent().is('.k-state-focused');

                            if (!isSelected) {
                                grid.select(td.parent());
                            }

                            self.choose();
                        } else if (evt.keyCode === kendo.keys.ESC) {
                            self.close();
                        }
                    })
                    .on('dblclick' + ns, '.k-grid td[role="gridcell"]', function() {
                        self.choose();
                    })
                    .on('keydown' + ns, SELECT_FIELDS, function(evt) {
                        if (evt.keyCode === kendo.keys.ENTER) {
                            $(evt.target).trigger(CHANGE);
                            self.filter();
                            evt.preventDefault();
                        }
                    });
            },

            _initDataSource: function() {
                var self = this;

                // Raise any previously registered callbacks
                self._raiseFilterCallbacks('Data source re-initialized', 0);

                if (self.dataSource && self._refreshHandler) {
                    self.dataSource.unbind(CHANGE, self._refreshHandler);
                } else {
                    self._refreshHandler = $.proxy(self._dataSourceChanged, self);
                }

                self.dataSource = kendo.data.DataSource.create(self.options.dataSource);
                self.dataSource.bind(CHANGE, self._refreshHandler);

                if(self._grid) {
                    self._grid.setDataSource(self.dataSource);
                }

                self._initColumns();

                if (self.options.autoBind) {
                    self.dataSource.fetch();
                }
            },

            _raiseFilterCallbacks: function(err, numItems) {
                var idx,
                    callback,
                    self = this;

                if (self._pendingCallbacks) {
                    for (idx = 0; idx < self._pendingCallbacks.length; idx++) {
                        callback = self._pendingCallbacks[idx];
                        callback(err, numItems);
                    }
                }
                self._pendingCallbacks = [];
            },

            _initColumns: function() {
                var self = this;

                if (self.options.fieldColumns.length) {
                    self._fieldColumns = self.options.fieldColumns;
                }
                else {
                    self._fieldColumns = [];
                }

                self._filterDefaultValues();
            },

            _initFields: function() {
                var self = this,
                    $container = self.window.element.find(SELECT_FIELDS),
                    column;

                if (!self._fieldColumns.length) {
                    $container.html(INVALID_DS_MSG);
                    return;
                }

                $container.html('');

                // Which fields are required? Used by the validators. It is written to when iterating the fields below.
                self._requiredFields = {};
                self._selectiveFields = {};

                // Create the fields HTML
                for (var field in self._fieldColumns) {
                    column = self._fieldColumns[field];

                    if (typeof(self.filterValues.get(column.field)) === 'undefined') {
                        self.filterValues.set(column.field, '');
                    }

                    var fieldMeta = $.extend({
                        tooltip: '',
                        label: column.field,
                        required: '',
                        defaultOperator: 'contains',
                        allowedOperators: ['eq', 'contains'],
                        field: column.field,
                        format: null,
                        editor: null
                    }, column);

                    var requiredIndicator = '';
                    if (fieldMeta.required === 'Required') {
                        requiredIndicator = ' <span title="'+self.options.requiredTooltip+'" class="'+CLS_DETAIL_FIELD_REQUIRED+'">*</span>';
                        self._requiredFields[column.field] = fieldMeta;
                    }
                    else if (fieldMeta.required === 'SelectivelyRequired') {
                        requiredIndicator = ' <span title="'+self.options.selectivelyRequiredTooltip+'" class="'+CLS_DETAIL_FIELD_SELECTIVEREQUIRED+'">**</span>';
                        self._selectiveFields[column.field] = fieldMeta;
                    }

                    var operatorOptions = self._createOperatorOptions(fieldMeta);
                    $container.append(
                        $('<div class="'+CLS_DETAIL_FIELD+'" />')
                            .attr('data-field', fieldMeta.field)
                            .append($('<label />')
                                .html(fieldMeta.label)
                                .attr('title', fieldMeta.tooltip)
                                .append(requiredIndicator)
                            )
                            .append($('<label />')
                                .html('Oh Hai')
                                .attr('data-for', fieldMeta.field)
                                .addClass('k-invalid-msg')
                            )
                            .append(' ')
                            .append($('<div class="'+CLS_DETAIL_FIELD_VALUE+'">')
                                .append($('<div />')
                                    .kendoEditable({
                                        fields: [{
                                            field: fieldMeta.field,
                                            format: fieldMeta.format,
                                            editor: fieldMeta.editor
                                        }],
                                        model: self.filterValues
                                    })
                                )
                            )
                            .append($('<div class="'+CLS_DETAIL_FIELD_VALUE+'">')
                                .append($('<input data-type="operator" value="' + fieldMeta.defaultOperator + '" />'))
                            )
                    );
                    var dd = $('[data-field="'+fieldMeta.field+'"] [data-type="operator"]', $container);
                    dd.kendoDropDownList({
                            dataTextField: 'displayName',
                            dataValueField: 'code',
                            dataSource: operatorOptions,
                            enable: operatorOptions.length > 1
                        });
                }
            },

            _createOperatorOptions: function(meta) {

                var options = [];

                for (var operIdx = 0; operIdx < allOperators.length; operIdx++) {
                    var oper = allOperators[operIdx];

                    // todo: insert default at start of array
                    for (var allowedIdx = 0; allowedIdx < meta.allowedOperators.length; allowedIdx++) {
                        var allowed = meta.allowedOperators[allowedIdx] || '';
                        if (allowed.toLowerCase() === oper.code) {
                            if (oper.code === meta.defaultOperator) {
                                options.splice(0,0,oper);
                            } else {
                                options.push(oper);
                            }
                        }
                    }
                }

                return options;
            },

            _showNotification: function(message) {
                var self = this;
                if (!self._notiElem) {
                    self._notiElem = $('<div />').appendTo($('body')).kendoNotification({
                        show: function(e) {
                            if (!$('.' + e.sender._guid)[1]) {
                                    var target = self.element.parent(),
                                        left = target.position().left,
                                        top = target.position().top + target.height() + 5;

                                    e.element.parent().css({left: left, top: top});
                                }
                            }
                        });
                }
                self._notiElem.data('kendoNotification').show(message, 'error');
            },

            _validate: function() {
                var validator = this.window.element
                    .find(SELECT_FIELDS)
                    .data('kendoValidator');

                return validator.validate();
            },

            _validateFields: function(input) {
                var self = this,
                    fieldName = input[0].name,
                    otherFields,
                    otherField;

                if ($.trim((input.val() || '')).length === 0) {
                    if (self._requiredFields[fieldName]) {
                        return false;
                    }

                    if (self._selectiveFields[fieldName]) {
                        otherFields = self.window.element.find(SELECT_FIELDS).find('input[name]');
                        for (var key in self._selectiveFields) {
                            otherField = otherFields.filter('[name="' + key + '"]');
                            if ($.trim((otherField.val() || '')).length !== 0) {
                                return true;
                            }
                        }

                        return false;
                    }
                }
                return true;
            },

            _validateFieldsMessage: function(input) {
                var self = this,
                    fieldName = input[0].name,
                    msg = null;

                if (self._requiredFields[fieldName]) {
                    msg = 'Mandatory field.';
                }
                else if (self._selectiveFields[fieldName]) {
                    var visibleFieldsElems = self.window.element.find(SELECT_FIELDS).find('input[name]');

                    var visibleFields = [];
                    visibleFieldsElems.each(function() {
                        if (self._selectiveFields[this.name]) {
                            visibleFields.push(this.name);
                        }
                    });
                    msg = 'At least one of: ' +
                        $.map(visibleFields, function(name) {
                                var item = self._selectiveFields[name];
                                // TODO: Take into account the options.columns...
                                return item.label || item.name || name;
                            }
                        ).join(', ') +
                        ' is required.';
                }

                return msg;
            },

            _getInput: function() {
                return this.element[0];
            },

            _filterDefaultValues: function() {
                var self = this;

                if (typeof(self.filterValues) === 'undefined') {
                    self.filterValues = kendo.observable(self.options.filterValues);
                }
                else {
                    self.filterValues.forEach(function(obj, key) {
                        if (typeof (self.options.filterValues[key]) !== 'undefined') {
                            self.filterValues.set(key, self.options.filterValues[key]);
                        }
                        else {
                            self.filterValues.set(key, null);
                        }
                    });
                }
            },

            _resizeGrid: function() {
                if (!this.window || !this.window.options.visible) {
                    return;
                }

                var grid = this._grid.element,
                    dataArea = grid.find('.k-grid-content'),
                    wrapper = this.window.element.children().first(),
                    newHeight = wrapper.outerHeight() - 2,
                    contentHeight = 0;

                wrapper.children().not('.k-grid').each(function() {
                    //console.log('  -= ' + $(this).attr('class') + ' -> ' + $(this).outerHeight(true));
                    newHeight -= $(this).outerHeight(true);
                });
                grid.children().not('.k-grid-content').each(function() { contentHeight += $(this).height(); });

                // What about margin/border of the grid element?
                newHeight -= grid.outerHeight() - grid.innerHeight();

                //console.log('wrapper.outerHeight: ' + wrapper.outerHeight());
                //console.log('grid.height: ' + newHeight);
                //console.log('dataArea.height: '+(newHeight - contentHeight));

                grid.height(newHeight);
                dataArea.height(newHeight - contentHeight);
            },

            setDataSource: function(dataSource) {
                this.options.dataSource = dataSource;
                this._initDataSource();
            },

            ensureWindow: function() {

                var self = this;

                if (!self.window) {
                    self._initWindow();
                    self._initFields();
                    self._initWindowEvents();
                    self._refreshCommands();
                }

                return self.window;
            },

            open: function(skipReset) {

                var self = this,
                    wdw = self.ensureWindow();

                self._inputChanged = false;
                if (!skipReset) {
                    self.reset();
                }
                self.refresh();

                if (!wdw.options.visible) {
                    wdw.center().open();
                }

                self._resizeGrid();

                return self;
            },

            close: function() {
                if (this.window) {
                    this.window.close();

                    return this;
                }
            },

            filter: function(callback) {

                var self = this,
                    ds = self.dataSource,
                    qry = {
                        filter: [],
                        sort: ds.sort(),
                        group: ds.group(),
                        page: ds.page(),
                        pageSize: ds.pageSize(),
                        aggregate: ds.aggregate()
                    },
                    wdw = self.ensureWindow();

                if (self._validate()) {
                    var inputFieldset = wdw.element.find(SELECT_FIELDS);
                    /*inputFieldset.find('[name]').each(function() {
                        var $this = $(this);
                        var key = $this.attr('name');
                        if (key && typeof(self.filterValues[key] !== 'undefined')) {
                            self.filterValues.set(key, $this.val());
                        }
                    });*/
                    self.filterValues.forEach(function(obj, key) {
                        if (obj) {
                            var operElem = inputFieldset.find('[data-field="'+key+'"] [data-type="operator"]');
                            var operator = 'contains';
                            if (operElem.length) {
                                operator = operElem.val();
                            }
                            qry.filter.push({ field: key, operator: operator, value: obj });
                        }
                    });

                    if (callback) {
                        self._pendingCallbacks.push(callback);
                    }
                    ds.query(qry);
                }
            },

            filterFromInput: function(callback) {
                var self = this,
                    value = self._getInput().value;

                self._filterDefaultValues();
                self.filterValues.set(self.options.valueField, value);

                if (value.length) {
                    self._inputChanged = true;
                    self.filter(callback);
                } else {
                    if (callback) {
                        callback();
                    }
                }

            },

            reset: function() {
                var self = this,
                    value = self.wrapper.find(SELECT_INPUT).val(),
                    wdw = self.ensureWindow(),
                    validator = wdw.element
                        .find(SELECT_FIELDS)
                        .data('kendoValidator');

                self._filterDefaultValues();

                if (value) {
                    self.filterValues.set(self.options.valueField, value);
                }

                if (self.dataSource) {
                    self.dataSource.data([]);
                }

                if (validator) {
                    validator.hideMessages();
                    wdw.element.find(SELECT_FIELDS).find('input.k-invalid').removeClass('k-invalid');
                }
            },

            choose: function() {
                var self = this;

                if (self.options.valuePrimitive) {
                    self._oldVal = self.value();
                }
                else {
                    self._oldVal = self.value()[self.options.valueField];
                }

                self.wrapper.find(SELECT_INPUT)
                    .val(self._oldVal)
                    .trigger(CHANGE);

                self.trigger(CHANGE);

                self.close();
            },

            destroy: function() {
                var that = this;

                that.element.off(ns);
                that.wrapper.off(ns);
                if (that.window) {
                    that.window.element.off(ns);
                }

                kendo.ui.Widget.fn.destroy.call(that);
            },

            refresh: function() {
                var self = this,
                    $elem = $(self.element);

                // Manipulate the DOM between the two events
                self.trigger(DATABINDING);

                if (true === self.options.showInput) {
                    $elem.show();
                } else {
                    $elem.hide();
                }

                if (!isNaN(parseInt(self.options.autoFocusTimeout))) {
                    setTimeout(function() { self.wrapper.find('button').focus(); }, parseInt(self.options.autoFocusTimeout));
                    self.options.autoFocusTimeout = null;
                }

                self._refreshCommands();

                self.trigger(DATABOUND);
            },

            _refreshCommands: function() {

                // Don't call ensureWindow in here, since it is called from refresh().
                if (!this.window) {
                    return;
                }

                var self = this,
                    btn = self.window.element.find(SELECT_BUTTON_OK);

                if (self.selected()) {
                    btn.removeClass(DISABLED);
                }
                else {
                    btn.addClass(DISABLED);
                }
            },

            _dataSourceChanged: function(ds) {
                var self = this;

                if (!ds.items) {
                    self._selected = null;
                    return self._raiseFilterCallbacks('No items in datasource change event callback.', 0);
                }

                if (self.window && self.window.options.visible) {
                    if (ds.items.length && self.options.enableFocus) {
                        self._grid.table.focus();
                    }
                } else {
                    if (self._inputChanged) {
                        if (ds.items.length !== 1) {
                            self._selected = null;
                            if (self.options.openOnInvalid) {
                                self.open(true);
                            } else if (self.options.notificationOnInvalid) {
                                self._showNotification(ds.items.length === 0 ? NO_RESULTS_MSG : TOO_MANY_RESULTS_MSG);
                            }

                            return self._raiseFilterCallbacks(ds.items.length + ' matching rows found.', ds.items.length);
                        } else {
                            self._selected = ds.items[0];
                            self.choose();

                            return self._raiseFilterCallbacks(null, ds.items.length);
                        }
                    }
                }

                this.refresh();
                this._resizeGrid();

                return self._raiseFilterCallbacks(null, ds.items.length);
            },

            value: function (value) {
                var self = this,
                    dataItem;

                if (value !== undefined) {
                    if (!self.options.valuePrimitive) {
                        value = value[self.options.valueField];
                    }
                    self._oldVal = value || '';
                    self.wrapper.find(SELECT_INPUT).val(self._oldVal);
                } else {
                    dataItem = self._accessor();
                    if (self.options.valuePrimitive) {
                        return (dataItem) ? dataItem[self.options.valueField] : null;
                    } else {
                        return (dataItem) ? dataItem : null;
                    }
                }
            },

            lovMeta: function(meta) {
                this.options.meta = meta;
                this._initDataSource();
            },

            selected: function() {
                return this._accessor();
            },

            _inputChange: function() {
                var self = this,
                    value = self.wrapper.find(SELECT_INPUT).val() || '';

                if (value !== self._oldVal) {
                    self.filterFromInput();

                    self._oldVal = value || '';
                }
            },

            _gridChange: function() {
                var self = this,
                    grid = self._grid,
                    selected = grid.select();

                if (selected.length) {
                    self._selected = grid.dataItem(selected[0]);
                } else {
                    self._selected = null;
                }

                self._refreshCommands();
            },

            _accessor: function (value) {
                if (value !== undefined) {
                    throw new Error('Can\'t set accessor on LOV control');
                } else {
                    return this._selected;
                }
            },

            options: {
                name: 'Lookup',
                placeholder: '',
                openOnInvalid: false,
                notificationOnInvalid: true,
                showInput: true,
                windowTitle: 'Search and Select',
                selectivelyRequiredTooltip: 'This, or any other field with the ** symbol, must be populated.',
                requiredTooltip: 'This field must be populated.',
                windowWidth: 600,
                windowHeight: 450,
                outerWrapClass: 'k-widget vm-lookup',
                innerWrapClass: 'k-picker-wrap k-state-default',
                autoBind: false,
                valueField: null,
                valuePrimitive: false,
                value: null,
                enableFocus: false, // Enable focusing input box and table after results are found. Causes problems when the page height exceeds browser clientHeight.

                // Which fields are visible
                fieldColumns: [],

                // Which fields are displayed in the grid
                gridColumns: [],

                // Current values of the filters
                filterValues: {},
                meta: {
                    // Not used currently.
                    'name': '',

                    // Not used currently.
                    'attrNames': null,

                    // Not used currently.
                    'derivedAttrNames': null,

                    // Not used currently.
                    'listDisplayAttrNames': null,
                    'displayCriteria': null,
                    'multiOperators': false,
                    'autoExecute': false
                },

                autoFocusTimeout: 1000
            },

            events: [
                DATABINDING,
                DATABOUND,
                CHANGE
            ],

            items: function() {
                return this.element.children();
            }
        });
        kendo.ui.plugin(Lookup);

        kendo.data.binders.widget.lovMeta = kendo.data.Binder.extend({
            'refresh': function() {
                var value = this.bindings.lovMeta.get();
                if (this.element.lovMeta) {
                    this.element.lovMeta(value);
                }
            }
        });

    })(window.kendo.jQuery);

    return window.kendo;

}, typeof define === 'function' && define.amd ? define : function(_, f){ f(); });

})();
(function() {

    'use strict';
    /* global define */

(function(f, define){
    define([], f);
})(function(){
    (function($) {
        var kendo = window.kendo = window.kendo || { cultures: {} };

        /** KENDO EXTENSION - Magic Lookup **/

        var DATABINDING = 'dataBinding',
            DATABOUND = 'dataBound',
            CHANGE = 'change',

            // TODO: Make this more generic and data driven, rather than hard coded column values here?.
            PAT_MRN_IDX = 0,
            PAT_AUID_IDX = 1,
            PAT_GNAME_IDX = 2,
            PAT_SNAME_IDX = 3,
            PAT_EP_START_IDX = 4,
            PAT_EP_FINISH_IDX = 5,

            /*PAT_MRN_IDX = 'mrn',
            PAT_GNAME_IDX = 'otherNames',
            PAT_SNAME_IDX = 'surnam',
            PAT_EP_START_IDX = 'admitDatetime',
            PAT_EP_FINISH_IDX = 'dischargeDatetime',
            */

            SELECT_INPUT = '[data-ml="input"]',
            FOCUSED = 'k-state-focused',
            ns = '.magicLookup';
        var MagicLookup = kendo.ui.Widget.extend({
            init: function(element, options) {
                var self = this;

                self.ns = ns;

                // Base initialization
                kendo.ui.Widget.fn.init.call(this, element, options);

                element = self.element;
                options = self.options;

                options.placeholder = options.placeholder || element.attr('placeholder');
                if (kendo.support.placeholder) {
                    element.attr('placeholder', options.placeholder);
                }

                self.wrapper = element.wrap('<div data-ml="wrapper" class="vm-magic-lookup" />').parent();
                element.attr('data-ml', 'input');
                element.addClass('k-input');

                self._initDataSource();

                self._initRowTemplate();
                self._initList();
                self._initPopup();
                self._initEvents();

                self._initStartDateTime();

                self._old = self._accessor();

                kendo.notify(self);
            },

            _initEvents: function() {

                var self = this,
                    $elem = $(self._getInput());

                if ($elem.length !== 1) {
                    return;
                }

                self.wrapper
                    .on('keyup' + ns, SELECT_INPUT, $.proxy(self._onKeyup, self))
                    .on('keydown' + ns, SELECT_INPUT, $.proxy(self._onKeydown, self))
                    .on('paste' + ns, SELECT_INPUT, $.proxy(self.search, self))
                    .on('focus' + ns, SELECT_INPUT, function () {
                        $elem.addClass(FOCUSED);
                    })
                    .on('blur' + ns, SELECT_INPUT, function () {
                        self._change();
                        $elem.removeClass(FOCUSED);
                    });
            },

            destroy: function() {
                var that = this;

                that.popup.destroy();

                that.element.off(ns);
                that.wrapper.off(ns);

                kendo.ui.Widget.fn.destroy.call(that);
            },

            _getInput: function() {
                return this.element[0];
            },

            _onKeydown: function(evt) {
                var self = this,
                    listView = self.matchesList,
                    listViewDs,
                    keys = kendo.keys,
                    isEnter = evt.keyCode === keys.ENTER,
                    isTab = evt.keyCode === keys.TAB,
                    isArrow = (37 <= evt.keyCode && evt.keyCode <= 40),
                    selectedIdx;

                if (!self.popup.visible()) {
                    return;
                }

                if (isEnter || isTab) {
                    listViewDs = listView.options.dataSource;
                    if (listViewDs.total() > 0) {
                        selectedIdx = Math.max(0, self._getSelectedRowIndex());
                        self._getInput().value = listViewDs.data()[selectedIdx].PatientLOVId;
                    }
                }

                if (isArrow) {
                    if (evt.keyCode === 38) {
                        // Up
                        self._moveSelectedRow(-1);
                    } else if (evt.keyCode === 40) {
                        // Down
                        self._moveSelectedRow(1);
                    }

                    self._lastSearch = self._getInput().value;
                    evt.preventDefault();
                    return;
                }

                if (isTab) {
                    self.hideFloats();
                }
            },

            _onKeyup: function(evt) {
                var self = this,
                    input = self._getInput(),
                    newVal = input.value,
                    keys = kendo.keys,
                    isEnter = evt.keyCode === keys.ENTER,
                    isEsc = evt.keyCode === keys.ESC;

                if (isEnter || isEsc) {
                    self.hideFloats();
                    input.setSelectionRange(newVal.length, newVal.length);

                    evt.preventDefault();

                    self._lastSearch = newVal;
                    return;
                }

                // The following test is a fix for cases of incredibly fast typing. Hit two number keys very quickly,
                // and by the time this event callback is hit the first time, 'newVal' will already have the value of both keys
                // in it. This means that we only need to handle the event once, even though it is fired twice. We detect this
                // by looking to see if the selection applied by the last event handle is still in effect.
                if (input.selectionEnd - input.selectionStart > 0) {
                    return;
                }

                self.search();
            },

            search: function() {
                var self = this,
                    input = self._getInput(),
                    newVal = input.value,
                    typed,
                    lastSearch = self._lastSearch || '',
                    matches;

                self._lastSearch = newVal;

                if (newVal.length === 0) {
                    self.hideFloats();
                    return;
                }

                if (lastSearch !== newVal) {
                    typed = newVal.length > lastSearch.length;

                    if (!isNaN(parseInt(newVal))) {
                        matches = self._search(newVal, function(row) {
                            return (row[PAT_MRN_IDX] || '').indexOf(newVal) === 0 || (row[PAT_AUID_IDX] || '').indexOf(newVal) === 0;
                        });
                        if (matches.length > 0 && typed) {

                            // Was it an MRN or AUID that matched in the first row?
                            var matchedValue = (matches[0][PAT_AUID_IDX] || '').indexOf(newVal) === 0 ? matches[0][PAT_AUID_IDX] : matches[0][PAT_MRN_IDX];
                            self._accessor(matchedValue);
                            self._getInput().setSelectionRange(newVal.length, matchedValue.length);
                        }
                    } else {
                        var regex = new RegExp(newVal + '.*', 'i');
                        matches = self._search(newVal, function(row) { return regex.test(row[PAT_SNAME_IDX]); });
                    }

                    self._updateMatchesList(matches, typed);
                }
            },

            _search: function(newVal, filterFn) {
                var self = this,
                    row,
                    len = self._dsView.length,
                    matches = [];

                for (var idx = 0; idx < len; idx++) {

                    row = self._dsView[idx];

                    // Does the filter pass?
                    if (filterFn(row, newVal)) {
                        // Within the period?
                        if (row[PAT_EP_START_IDX] <= self._startDateTime && (row[PAT_EP_FINISH_IDX] === null || self._startDateTime <= row[PAT_EP_FINISH_IDX])) {

                            // Intentionally allow pageSize + 1 entries, so we know to display the '...' at the bottom
                            // of the list.
                            matches.push(row);
                            if (matches.length > self.options.pageSize) {
                                break;
                            }
                        }
                    }
                }

                return matches;
            },

            _onMatchListViewChange: function(/* evt */) {
                var self = this,
                    input, inputValue,
                    listView = self.matchesList,
                    selectedIndex,
                    view,
                    mrn,
                    auid;

                selectedIndex = $(listView.select()[0]).index();
                view = listView.options.dataSource.view();
                if (selectedIndex !== null && selectedIndex >= 0 && view.length > selectedIndex) {
                    input = self._getInput();
                    inputValue = input.value || '';
                    if (!isNaN(parseInt(input.value))) {
                        mrn = view[selectedIndex].PatientMRN;
                        auid = view[selectedIndex].PatientAUID;
                        if (inputValue !== mrn && inputValue.indexOf(mrn) === 0) {
                            $(input).val(mrn);
                            input.setSelectionRange(0, mrn.length);
                        } else if (inputValue !== auid && inputValue.indexOf(auid) === 0) {
                            $(input).val(auid);
                            input.setSelectionRange(0, auid.length);
                        }
                    }
                }
            },

            refresh: function() {
                var self = this;

                self._lastSearch = '';
                self._dsView = this.dataSource.view();

                self._initStartDateTime();

                // Manipulate the DOM between the two events
                self.trigger(DATABINDING);

                if (!isNaN(parseInt(self.options.autoFocusTimeout))) {
                    setTimeout(function() { self._getInput().focus(); }, parseInt(self.options.autoFocusTimeout));
                }

                self.trigger(DATABOUND);
            },

            hideFloats: function() {
                var self = this;

                self.popup.close();
                /*
                self.wrapper.find(SELECT_FLOATS).hide();

                if (!self._bodyFocusinHandler)
                    self._bodyFocusinHandler = $.proxy(self._onBodyFocusin, self);
                self.wrapper.off('focusin', self._bodyFocusinHandler);
                */
            },

            showFloats: function() {
                var self = this;

                self.popup.open();
                /*
                self.wrapper.find(SELECT_FLOATS).show();

                if (!self._bodyFocusinHandler)
                    self._bodyFocusinHandler = $.proxy(self._onBodyFocusin, self);
                self.wrapper.on('focusin', self._bodyFocusinHandler);
            },

            _onBodyFocusin: function(evt) {
                if ($(evt.target).parents('[data-ml]').length === 0)
                    this.hideFloats();
                */
            },

            _updateMatchesList: function(matches, allowRowSelection) {
                var self = this,
                    newData = [],
                    oldData,
                    ds,
                    idx,
                    match;

                ds = self.matchesList.options.dataSource;

                if (!self.popup.visible()) {
                    self.showFloats();
                }

                // Same list?
                oldData = ds.view();
                if (matches.length === oldData.length) {
                    for (idx = 0; idx < matches.length; idx++) {
                        if (matches[idx][PAT_MRN_IDX] !== oldData[idx].PatientLOVId) {
                            oldData = null;
                            break;
                        }
                    }
                    if (oldData !== null) {
                        if (allowRowSelection) {
                            var selectedIdx = self._getSelectedRowIndex();
                            if (selectedIdx === -1) {
                                self._moveSelectedRow(1);
                            }
                        }

                        // List is the same
                        return;
                    }
                }

                for (idx = 0; idx < matches.length; idx++) {
                    match = matches[idx];
                    newData.push({
                        PatientLOVId: match[PAT_MRN_IDX] || match[PAT_AUID_IDX],
                        PatientMRN: match[PAT_MRN_IDX],
                        PatientAUID: match[PAT_AUID_IDX],
                        PatientGivenName: match[PAT_GNAME_IDX],
                        PatientSurname: match[PAT_SNAME_IDX],
                        EpisodeStartDate: match[PAT_EP_START_IDX],
                        EpisodeFinishDate: match[PAT_EP_FINISH_IDX]
                    });
                }

                ds.data(newData);
                if (matches.length === 0) {
                    self.noMatches.show();
                    self.matchesList.element.hide();
                    self.matchesMore.hide();
                } else {
                    self.noMatches.hide();
                    self.matchesList.element.show();

                    if (ds.totalPages() > 1) {
                        self.matchesMore.show();
                    }
                    else {
                        self.matchesMore.hide();
                    }

                    if (allowRowSelection) {
                        self._resetSelectedRow();
                    }
                }

                this.showFloats();
            },

            _moveSelectedRow: function(movement) {
                var listView = this.matchesList,
                    selectedItems,
                    currentIdx;

                selectedItems = listView.select();
                if (selectedItems.length === 0) {
                    currentIdx = -1;
                }
                else {
                    currentIdx = $(selectedItems[0]).index();
                }

                currentIdx = (currentIdx + movement + listView.element.children().length) % listView.element.children().length;
                listView.select(listView.element.children()[currentIdx]);

                // Make sure the popup is visible, then return.
                this.showFloats();
            },

            _getSelectedRowIndex: function() {
                var listView = this.matchesList,
                    selectedItems;

                selectedItems = listView.select();
                if (selectedItems.length === 0) {
                    return -1;
                }
                return $(selectedItems[0]).index();
            },

            _resetSelectedRow: function() {
                var listView = this.matchesList,
                    children;

                children = $(listView.element).children();
                if (children.length > 0) {
                    listView.select(children[0]);
                }

            },

            value: function (value) {
                if (value !== undefined) {
                    this._accessor(value);
                    this._old = value;
                    this._lastSearch = '';
                } else {
                    return this._accessor();
                }
            },

            _change: function() {
                var self = this,
                    value = self._accessor();

                if (value !== self._old) {
                    self._old = value;

                    self.trigger(CHANGE);

                    // trigger the DOM change event so any subscriber gets notified
                    self.element.trigger(CHANGE);
                }
            },

            _accessor: function (value) {
                var self = this,
                    element = self.element[0];

                if (value !== undefined) {
                    element.value = value === null ? '' : value;
                    //that._placeholder();
                } else {
                    value = element.value;

                    if (element.className.indexOf('k-readonly') > -1) {
                        if (value === self.options.placeholder) {
                            return '';
                        } else {
                            return value;
                        }
                    }

                    return value;
                }
            },

            setDataSource: function(dataSource) {
                this.options.dataSource = dataSource;
                this._initDataSource();
            },

            setLineStartDate: function(startDate) {
                this.options.lineStartDate = startDate;
                this._initStartDateTime();
            },

            setLineStartTime: function(startTime) {
                this.options.lineStartTime = startTime;
                this._initStartDateTime();
            },

            _initList: function() {
                var self = this;

                /* Hierarchy:
                div                                 (self.wrapper)
                  input[data-ml="input"]            (self.element)
                  div[data-ml="floats"]
                    div[data-ml="matches"]
                    div[data-ml="hint"]
                    div[data-ml="no-matches"]
                    div[data-ml="more-matches"]
                */

                // Create our floats wrapper
                self.floats = $('<div data-ml="floats" class="vm-ml-floats k-tooltip"></div>');

                // Create our other elements
                self.matchesList = $('<div data-ml="matches" class="vm-ml-matches"></div>')
                    .kendoListView({
                        template: self._rowTemplate,
                        dataSource: new kendo.data.DataSource({ data: [], pageSize: self.options.pageSize }),
                        selectable: 'single',
                        change: $.proxy(self._onMatchListViewChange, self)
                    })
                    .dblclick($.proxy(self.hideFloats, self))
                    .data('kendoListView');

                self.hintText = $('<div data-ml="hint" class="vm-ml-hinttext">' +
                    self.options.hintMessage +
                    '</div>');

                self.noMatches = $('<div data-ml="no-matches" class="vm-ml-hinttext vm-ml-no-matches">' +
                    self.options.noMatchesMessage +
                    '</div>');

                self.matchesMore = $('<div data-ml="more-matches" class="vm-ml-hinttext">' +
                    self.options.moreMatchesMessage.replace('{0}', self.options.pageSize) +
                    '</div>');

                // Insert them into our floats wrapper
                self.floats.append(self.matchesList.element);
                self.floats.append(self.hintText);
                self.floats.append(self.noMatches);
                self.floats.append(self.matchesMore);
            },

            _initPopup: function() {
                var that = this,
                    //focused = that._focused,
                    options = that.options,
                    wrapper = that.wrapper;

                that.popup = new kendo.ui.Popup(that.floats, $.extend({}, options.popup, {
                    anchor: wrapper,
                    open: function(/* e */) {
                    },
                    close: function(/* e */) {
                    },
                    animation: options.animation,
                    isRtl: kendo.support.isRtl(wrapper)
                }));

                that._touchScroller = kendo.touchScroller(that.popup.element);
            },

            _initDataSource: function() {
                var self = this;

                self._dsView = [];

                if (self.dataSource && self._refreshHandler) {
                    self.dataSource.unbind(CHANGE, self._refreshHandler);
                } else {
                    self._refreshHandler = $.proxy(self.refresh, self);
                }

                if (!(self.options.dataSource instanceof kendo.data.DataSource)) {
                    self.dataSource = kendo.data.DataSource.create(self.options.dataSource);
                }
                else {
                    self.dataSource = self.options.dataSource;
                }

                self.refresh();
                self.dataSource.bind(CHANGE, self._refreshHandler);

                if (self.options.autoBind) {
                    self.dataSource.fetch();
                }
            },

            _initStartDateTime: function() {
                var self = this;
                if (!self.options.lineStartDate) {
                    throw new Error('Invalid line start date');
                }
                self._startDateTime = self._toDateFromParts(self.options.lineStartDate, self.options.lineStartTime).getTime();
            },

            _initRowTemplate: function() {
                this._rowTemplate = kendo.template(
                    this.options.rowTemplate ||
                    '<div class="vm-ml-row">'+
                    '  <div class="vm-ml-row-primary">#= data[\'' + (this.options.rowField || '') + '\'] #</div>'+
                    '</div>'
                );
            },

            _toDateFromParts: function (date, time) {

              var newDate = new Date(date.getTime()),
                match = time.match(/^(\d{1,2}):(\d{2})$/);

              if (match) {
                newDate.setHours(match[1]);
                newDate.setMinutes(match[2]);
              } else {
                newDate.setHours(0);
                newDate.setMinutes(0);
              }

              return newDate;
            },

            options: {
                name: 'MagicLookup',
                autoBind: true,
                pageSize: 5,
                lineStartDate: new Date(),
                lineStartTime: '00:00',
                hintMessage: 'Searching patients already entered in this claim.',
                noMatchesMessage: 'No matching patients available.',
                moreMatchesMessage: 'Only showing first {0} results.',
                placeholder: '',
                rowTemplate: null, // kendo Template object that is used to render each results row.
                rowField: null, // the name of the object member on each result row that is displayed, if no rowTemplate is provided.
                autoFocusTimeout: null,
                value: null,
                animation: {}
            },

            events: [
                DATABINDING,
                DATABOUND,
                CHANGE
            ],

            items: function() {
                return this.element.children();
            }
        });
        kendo.ui.plugin(MagicLookup);

})(window.kendo.jQuery);

return window.kendo;

}, typeof define === 'function' && define.amd ? define : function(_, f){ f(); });

})();